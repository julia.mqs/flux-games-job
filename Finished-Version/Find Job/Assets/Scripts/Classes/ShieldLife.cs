using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldLife : MonoBehaviour
{
    public Slider slider;

    public void setMaxShield (int lifeShield)
    {
        slider.maxValue = lifeShield;
        slider.value = lifeShield;
    }

    public void setShield (int lifeShield)
    {
        slider.value = lifeShield;
    }
}
