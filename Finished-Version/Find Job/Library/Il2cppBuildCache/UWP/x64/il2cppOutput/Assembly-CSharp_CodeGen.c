﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Life::TakesLife(System.Int32)
extern void Life_TakesLife_m432769A0DBB59D454784B3402F19D358B66CBDBD (void);
// 0x00000002 System.Void Life::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Life_OnTriggerEnter2D_mB5E33226D7DB91144ACC54786FDF08388B017178 (void);
// 0x00000003 System.Void Life::.ctor()
extern void Life__ctor_m7EA475E113F8E58CDBD7F64DE7C3378510DD7926 (void);
// 0x00000004 System.Void ShieldLife::setMaxShield(System.Int32)
extern void ShieldLife_setMaxShield_mC8E0EF88AA7A04B33BCB5831794D9F629C71FF7F (void);
// 0x00000005 System.Void ShieldLife::setShield(System.Int32)
extern void ShieldLife_setShield_m9495FF985111077CF0682E50324F8C5DFFC52907 (void);
// 0x00000006 System.Void ShieldLife::.ctor()
extern void ShieldLife__ctor_mE4A6C282A2616BA5D40F1CC8CB105E50386A0CBB (void);
// 0x00000007 System.Void ControlGame::Start()
extern void ControlGame_Start_m41EDFA823653EE35B75C9E396B158F9C47330225 (void);
// 0x00000008 System.Void ControlGame::Update()
extern void ControlGame_Update_m75CA34A327D9AE3F5BE315C068BB2D3946985B50 (void);
// 0x00000009 System.Void ControlGame::LevelPassed()
extern void ControlGame_LevelPassed_m05B6235DD8F835987CA547397293A735BB6F6534 (void);
// 0x0000000A System.Void ControlGame::GameOver()
extern void ControlGame_GameOver_m1E16954A2EB5306CA5D7959354BD4728C0321A13 (void);
// 0x0000000B System.Void ControlGame::Clear()
extern void ControlGame_Clear_m0681179893AC0309B4E59F5EA1033BE470C10074 (void);
// 0x0000000C System.Void ControlGame::.ctor()
extern void ControlGame__ctor_mBB7334A9B707AA5AB4EC6F7C853BE92433BB4D4D (void);
// 0x0000000D System.Void ControlStart::Start()
extern void ControlStart_Start_m201D1963F24351E276FC706916096DE032376F51 (void);
// 0x0000000E System.Void ControlStart::StartClick()
extern void ControlStart_StartClick_m4F919D6E668B781C8C5DC0EB2EB21611D882EFAB (void);
// 0x0000000F System.Void ControlStart::Quit()
extern void ControlStart_Quit_mD819D0CAD1831F9E534C96E210F5FFFF9FDB1747 (void);
// 0x00000010 System.Void ControlStart::.ctor()
extern void ControlStart__ctor_mB1F89FE5B8F09B1F79FC79EA5BFDBDCEBEF790EA (void);
// 0x00000011 System.Void EnemyControl::Start()
extern void EnemyControl_Start_mB2709A00C33F64E801FB336D9224A48BAEDF451F (void);
// 0x00000012 System.Collections.IEnumerator EnemyControl::Process()
extern void EnemyControl_Process_m3026B4978A8349BD60037F2B25AFA4539B70FB7B (void);
// 0x00000013 System.Void EnemyControl::EnemyCreate()
extern void EnemyControl_EnemyCreate_m1DBC2B3B2C19C6C6D410EAB1360BB814DD12FD3E (void);
// 0x00000014 System.Collections.IEnumerator EnemyControl::CallBoss()
extern void EnemyControl_CallBoss_mEE1F75478CABCE55BC6A16FA00D0F843E7A58101 (void);
// 0x00000015 System.Void EnemyControl::.ctor()
extern void EnemyControl__ctor_mE15FA6B97AEFE883D943CC0D237897F435BC34BF (void);
// 0x00000016 System.Void EnemyControl/<Process>d__4::.ctor(System.Int32)
extern void U3CProcessU3Ed__4__ctor_mA6DCD4EAD0188CA4C4AD0A5EF0D2A9FC2F8ADC15 (void);
// 0x00000017 System.Void EnemyControl/<Process>d__4::System.IDisposable.Dispose()
extern void U3CProcessU3Ed__4_System_IDisposable_Dispose_m382FC3C37C2241AC4E64B8D0EC2C821F916C3DE2 (void);
// 0x00000018 System.Boolean EnemyControl/<Process>d__4::MoveNext()
extern void U3CProcessU3Ed__4_MoveNext_m3A08B74C2B6D96C327D949D051A8EA8D5B423DDC (void);
// 0x00000019 System.Object EnemyControl/<Process>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m277414C234EE05878133959D21839B5949AC295B (void);
// 0x0000001A System.Void EnemyControl/<Process>d__4::System.Collections.IEnumerator.Reset()
extern void U3CProcessU3Ed__4_System_Collections_IEnumerator_Reset_mE513AF299BB9BF436B27408FF4C0558A73F4676D (void);
// 0x0000001B System.Object EnemyControl/<Process>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CProcessU3Ed__4_System_Collections_IEnumerator_get_Current_m158EF3608628631AC8F9A0A6CE2568AEB6566996 (void);
// 0x0000001C System.Void EnemyControl/<CallBoss>d__6::.ctor(System.Int32)
extern void U3CCallBossU3Ed__6__ctor_mDA19F5EC5865BAF920AD4E709D8ADE95E743F363 (void);
// 0x0000001D System.Void EnemyControl/<CallBoss>d__6::System.IDisposable.Dispose()
extern void U3CCallBossU3Ed__6_System_IDisposable_Dispose_m68D5010133251BF57F27F51D1BB928B1E4C8DDD9 (void);
// 0x0000001E System.Boolean EnemyControl/<CallBoss>d__6::MoveNext()
extern void U3CCallBossU3Ed__6_MoveNext_mDF3544E995D32004659624A65FB35CCD76C0B909 (void);
// 0x0000001F System.Object EnemyControl/<CallBoss>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCallBossU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2297F4117C95DDE24CFD237A355224230FA00BD9 (void);
// 0x00000020 System.Void EnemyControl/<CallBoss>d__6::System.Collections.IEnumerator.Reset()
extern void U3CCallBossU3Ed__6_System_Collections_IEnumerator_Reset_mAB176BC3F3DE4AD36A0EAF25D234733D5CA47B3D (void);
// 0x00000021 System.Object EnemyControl/<CallBoss>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CCallBossU3Ed__6_System_Collections_IEnumerator_get_Current_mB515EAE6DED7E678D67D764943739ADEF1C84D27 (void);
// 0x00000022 System.Void RecordControl::Start()
extern void RecordControl_Start_m8F7A53F239DE398E63E55AA2A9EF390392EB516F (void);
// 0x00000023 System.Void RecordControl::UpdateName()
extern void RecordControl_UpdateName_m3C7C5ABFF677519820B6F281AF99A768064360DC (void);
// 0x00000024 System.Void RecordControl::.ctor()
extern void RecordControl__ctor_m8B9BD294AC3D1EDA3869E09F7EFC58F7B97304C4 (void);
// 0x00000025 System.Void Boss2::Start()
extern void Boss2_Start_mF5292CA924E8DB443334DB78CCD7201F768E15C8 (void);
// 0x00000026 System.Collections.IEnumerator Boss2::ShowParts()
extern void Boss2_ShowParts_mCDC314F0FBDB8D208165FAAE3F8F4D358F7FD315 (void);
// 0x00000027 System.Void Boss2::Update()
extern void Boss2_Update_m8CE583E0DAC32C0A374F69542AD8C054754C0C14 (void);
// 0x00000028 System.Collections.IEnumerator Boss2::Shot()
extern void Boss2_Shot_m35B8B00F7AEF10F996EF1D55BEF3575E5BAFF3D6 (void);
// 0x00000029 UnityEngine.GameObject Boss2::GetOneActive()
extern void Boss2_GetOneActive_m41CEB8C5D4F936E741F9C9F684F40EAE7D3DADA3 (void);
// 0x0000002A System.Void Boss2::KillMe(UnityEngine.GameObject)
extern void Boss2_KillMe_mDE3ED52B5D597F8CEB197D98E1F15E9334C71285 (void);
// 0x0000002B System.Void Boss2::.ctor()
extern void Boss2__ctor_mAF0CEE4D38FEAFC1DE100B0F2E0701E06B14991F (void);
// 0x0000002C System.Void Boss2/<ShowParts>d__5::.ctor(System.Int32)
extern void U3CShowPartsU3Ed__5__ctor_mBDD0E8898D91E138B7042D22F0A5E1456EA501C8 (void);
// 0x0000002D System.Void Boss2/<ShowParts>d__5::System.IDisposable.Dispose()
extern void U3CShowPartsU3Ed__5_System_IDisposable_Dispose_m0AC2D2E39174DA2D173BDA8759091EEE1D71F146 (void);
// 0x0000002E System.Boolean Boss2/<ShowParts>d__5::MoveNext()
extern void U3CShowPartsU3Ed__5_MoveNext_m0B40D4DCC0A1EBAC248878B225A64D4925E31704 (void);
// 0x0000002F System.Object Boss2/<ShowParts>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F4521C1346516C77ED9C7B550671F928987CE41 (void);
// 0x00000030 System.Void Boss2/<ShowParts>d__5::System.Collections.IEnumerator.Reset()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m0468FE7B0426062B26C538D23C7C5FF8CC38570D (void);
// 0x00000031 System.Object Boss2/<ShowParts>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_m20F3D7FA705381FA5AA9C2B5C037BDCB2BC2128A (void);
// 0x00000032 System.Void Boss2/<Shot>d__7::.ctor(System.Int32)
extern void U3CShotU3Ed__7__ctor_mE5DADDE0420441F86985C44BD718DD08A4AD334C (void);
// 0x00000033 System.Void Boss2/<Shot>d__7::System.IDisposable.Dispose()
extern void U3CShotU3Ed__7_System_IDisposable_Dispose_m8F588491D89FF21288555692FE3A90E505E341F7 (void);
// 0x00000034 System.Boolean Boss2/<Shot>d__7::MoveNext()
extern void U3CShotU3Ed__7_MoveNext_m6010D4868EF31AE0774066824F01233F5F37D4AE (void);
// 0x00000035 System.Object Boss2/<Shot>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12B604A9AC558372662A0FFD180A325ACDD91EA6 (void);
// 0x00000036 System.Void Boss2/<Shot>d__7::System.Collections.IEnumerator.Reset()
extern void U3CShotU3Ed__7_System_Collections_IEnumerator_Reset_mE289E17958922FDDCC8DE7C1FA61940025274E62 (void);
// 0x00000037 System.Object Boss2/<Shot>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CShotU3Ed__7_System_Collections_IEnumerator_get_Current_m935EF7A6B9356B060AA9DFD3E78AA608C890BC25 (void);
// 0x00000038 System.Void Boss3::Start()
extern void Boss3_Start_m025FF1DBDC22B6304FBDBAD41E2A028B5BCF4E2C (void);
// 0x00000039 System.Collections.IEnumerator Boss3::ShowParts(System.Boolean)
extern void Boss3_ShowParts_m237CC1FA5AE12E3DC8B0F2E864759C88F37E1EC6 (void);
// 0x0000003A System.Collections.IEnumerator Boss3::AttackNow()
extern void Boss3_AttackNow_m751CAFCD3CD70F4748E1BE4A61688B418BF83420 (void);
// 0x0000003B System.Collections.IEnumerator Boss3::Attack(UnityEngine.UI.Image)
extern void Boss3_Attack_mBC827254E14F721046A48C74FBDCE480D7EB8006 (void);
// 0x0000003C UnityEngine.GameObject Boss3::GetOneActive()
extern void Boss3_GetOneActive_mC454EE4919EB35E3CADF35A2C047B267CBB5754F (void);
// 0x0000003D System.Void Boss3::KillMe(UnityEngine.GameObject)
extern void Boss3_KillMe_m485F8403E9462C6F16E4A1E1346373AE8AE15029 (void);
// 0x0000003E System.Void Boss3::.ctor()
extern void Boss3__ctor_m643E6AD2933C0D491F1706E2F4CA3BA2F3820306 (void);
// 0x0000003F System.Void Boss3/<ShowParts>d__4::.ctor(System.Int32)
extern void U3CShowPartsU3Ed__4__ctor_m070F5DA201A9A67A2D4EB009D50CEFA4CDF325CD (void);
// 0x00000040 System.Void Boss3/<ShowParts>d__4::System.IDisposable.Dispose()
extern void U3CShowPartsU3Ed__4_System_IDisposable_Dispose_m095CFB0C977C6035FE63D564EA6AB2576AF732F9 (void);
// 0x00000041 System.Boolean Boss3/<ShowParts>d__4::MoveNext()
extern void U3CShowPartsU3Ed__4_MoveNext_m04E0F92B3885C3D0069909C43E601567B8254F35 (void);
// 0x00000042 System.Object Boss3/<ShowParts>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowPartsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3AB3BC8537AF41519422CE94D13E117A2055BF4E (void);
// 0x00000043 System.Void Boss3/<ShowParts>d__4::System.Collections.IEnumerator.Reset()
extern void U3CShowPartsU3Ed__4_System_Collections_IEnumerator_Reset_m9C7BFE5CCD4D8826B79B86BA66548260D7BBDE04 (void);
// 0x00000044 System.Object Boss3/<ShowParts>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CShowPartsU3Ed__4_System_Collections_IEnumerator_get_Current_m1877C089084E4ED7D969D798AE14C8CEF88C76C6 (void);
// 0x00000045 System.Void Boss3/<AttackNow>d__5::.ctor(System.Int32)
extern void U3CAttackNowU3Ed__5__ctor_mCFE69ED94B71BE7330D7DBD55F859132EFA4D2F3 (void);
// 0x00000046 System.Void Boss3/<AttackNow>d__5::System.IDisposable.Dispose()
extern void U3CAttackNowU3Ed__5_System_IDisposable_Dispose_m89B4264BD52CD77D67EA94A14771AB7F55DEF46E (void);
// 0x00000047 System.Boolean Boss3/<AttackNow>d__5::MoveNext()
extern void U3CAttackNowU3Ed__5_MoveNext_m12010287B84893CC8526EBD2A04624722F2290D0 (void);
// 0x00000048 System.Object Boss3/<AttackNow>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttackNowU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4FFA34D632DB5A73E42B8EAD8D4861CA70617CF3 (void);
// 0x00000049 System.Void Boss3/<AttackNow>d__5::System.Collections.IEnumerator.Reset()
extern void U3CAttackNowU3Ed__5_System_Collections_IEnumerator_Reset_mAC62F325D898ABA3D347C2BE92E5A14D0B9299B0 (void);
// 0x0000004A System.Object Boss3/<AttackNow>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CAttackNowU3Ed__5_System_Collections_IEnumerator_get_Current_m132D1DD78E6BC1C54A09ED6F8AE816E2EFD154A2 (void);
// 0x0000004B System.Void Boss3/<Attack>d__6::.ctor(System.Int32)
extern void U3CAttackU3Ed__6__ctor_mA4F7019DA5183D416ADAAF260A4C975A61B43FDA (void);
// 0x0000004C System.Void Boss3/<Attack>d__6::System.IDisposable.Dispose()
extern void U3CAttackU3Ed__6_System_IDisposable_Dispose_m4A06ED756364F4E6E18CF74382173F30DA1633E3 (void);
// 0x0000004D System.Boolean Boss3/<Attack>d__6::MoveNext()
extern void U3CAttackU3Ed__6_MoveNext_mBA62607420F65E09A22036D8DD18C7F73B4F0AE2 (void);
// 0x0000004E System.Object Boss3/<Attack>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttackU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F2BD237C0E43E3D21F19E3DDE8AF264B68E03DE (void);
// 0x0000004F System.Void Boss3/<Attack>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAttackU3Ed__6_System_Collections_IEnumerator_Reset_m3555C671D642A35E37C61E8B3199D7C54B6C3F79 (void);
// 0x00000050 System.Object Boss3/<Attack>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAttackU3Ed__6_System_Collections_IEnumerator_get_Current_m2AA0E8B6B15E5E3BB4B4FE4A985F94E33AD15757 (void);
// 0x00000051 System.Void Enemy::Start()
extern void Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02 (void);
// 0x00000052 System.Void Enemy::Update()
extern void Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2 (void);
// 0x00000053 System.Void Enemy::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12 (void);
// 0x00000054 System.Void Enemy::MyDeath()
extern void Enemy_MyDeath_mA4085ED4BD10E6B3EDEE186807D3C8FB762A4CC8 (void);
// 0x00000055 System.Collections.IEnumerator Enemy::KillMe()
extern void Enemy_KillMe_m24D0CC29C673187902BFD4554072865ADF3F2EB5 (void);
// 0x00000056 System.Void Enemy::Create(System.Int32)
extern void Enemy_Create_m66ACA885B980722A913D038190E87E6AFFA55759 (void);
// 0x00000057 System.Collections.IEnumerator Enemy::Shoot()
extern void Enemy_Shoot_m9698649D8FCE4A907B5EBAA30105E3DF881FB767 (void);
// 0x00000058 System.Void Enemy::EndBoss()
extern void Enemy_EndBoss_mA358579DEF2D3DAF68CD80B9AD4C96C1981554DA (void);
// 0x00000059 System.Void Enemy::PStick()
extern void Enemy_PStick_m5BAC91E00684770270D3B5164328B2EF020D9173 (void);
// 0x0000005A System.Void Enemy::.ctor()
extern void Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734 (void);
// 0x0000005B System.Void Enemy/<KillMe>d__10::.ctor(System.Int32)
extern void U3CKillMeU3Ed__10__ctor_mCBE3F42CF5E6BEB5E774F037B0444C559C24EC4C (void);
// 0x0000005C System.Void Enemy/<KillMe>d__10::System.IDisposable.Dispose()
extern void U3CKillMeU3Ed__10_System_IDisposable_Dispose_m2DB73B1C1F038ACD13017517E648F7BC34FE75A7 (void);
// 0x0000005D System.Boolean Enemy/<KillMe>d__10::MoveNext()
extern void U3CKillMeU3Ed__10_MoveNext_m2B827634D8CEADE16F1ABA524332C1D91AAEEA08 (void);
// 0x0000005E System.Object Enemy/<KillMe>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKillMeU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB162D3E0999042813FBE550CD7FD7926C498BF3C (void);
// 0x0000005F System.Void Enemy/<KillMe>d__10::System.Collections.IEnumerator.Reset()
extern void U3CKillMeU3Ed__10_System_Collections_IEnumerator_Reset_m144E9C6610E020F7905EF9E9E55E6D769047778C (void);
// 0x00000060 System.Object Enemy/<KillMe>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CKillMeU3Ed__10_System_Collections_IEnumerator_get_Current_m89F0A6CE9070973A816212BD6572DA6DC9D9F303 (void);
// 0x00000061 System.Void Enemy/<Shoot>d__12::.ctor(System.Int32)
extern void U3CShootU3Ed__12__ctor_m4884E34148F61D16DCBE9E6C9E29CF766636E036 (void);
// 0x00000062 System.Void Enemy/<Shoot>d__12::System.IDisposable.Dispose()
extern void U3CShootU3Ed__12_System_IDisposable_Dispose_m9E7ADDC293233521A708C4B1114000B969F4F6E4 (void);
// 0x00000063 System.Boolean Enemy/<Shoot>d__12::MoveNext()
extern void U3CShootU3Ed__12_MoveNext_mD258E3C08A9B604CFA4F85EFA5B6E32189C9EC55 (void);
// 0x00000064 System.Object Enemy/<Shoot>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShootU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3E2421A685652897F0F5F6D05DA25B8ACDEE5DD (void);
// 0x00000065 System.Void Enemy/<Shoot>d__12::System.Collections.IEnumerator.Reset()
extern void U3CShootU3Ed__12_System_Collections_IEnumerator_Reset_m5C552696876E029CE03DD55B746CD7422330D2DF (void);
// 0x00000066 System.Object Enemy/<Shoot>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CShootU3Ed__12_System_Collections_IEnumerator_get_Current_m9D53A9EAB026124E6E8AF1E6BC87C3375A604866 (void);
// 0x00000067 System.Void ItemDrop::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ItemDrop_OnTriggerEnter2D_m1A1B2CBD8DABD7C2C8C666248C9E7B9A56F022A7 (void);
// 0x00000068 System.Void ItemDrop::.ctor()
extern void ItemDrop__ctor_m5D826B2329C00417D6FB12D4C40DE3EB0DB55AF2 (void);
// 0x00000069 System.Void CallScene::Call(System.String)
extern void CallScene_Call_m8CDF9D77505FAEDF29068B890862D09654434CCE (void);
// 0x0000006A System.Void CallScene::.ctor()
extern void CallScene__ctor_m6B52AD5FB87324D006A0BFF36D122D8E5096AE9C (void);
// 0x0000006B System.Void DestroyTime::Start()
extern void DestroyTime_Start_m8C0A193A37746E7A2A2C617E998A31E724373497 (void);
// 0x0000006C System.Collections.IEnumerator DestroyTime::CallDestroy()
extern void DestroyTime_CallDestroy_m61D5F2B05283C2474DC66285AC88FDA79B8E1BD6 (void);
// 0x0000006D System.Void DestroyTime::.ctor()
extern void DestroyTime__ctor_mAB5E312F1EAAD1B3B69CC64F2B95B607F8EE4F88 (void);
// 0x0000006E System.Void DestroyTime/<CallDestroy>d__2::.ctor(System.Int32)
extern void U3CCallDestroyU3Ed__2__ctor_m7A7A8DC06CB12FA979DC587DA59CD00B21710FAF (void);
// 0x0000006F System.Void DestroyTime/<CallDestroy>d__2::System.IDisposable.Dispose()
extern void U3CCallDestroyU3Ed__2_System_IDisposable_Dispose_m93E09EB5836B606D9F59C3CB667BDD2C8F1215BD (void);
// 0x00000070 System.Boolean DestroyTime/<CallDestroy>d__2::MoveNext()
extern void U3CCallDestroyU3Ed__2_MoveNext_mCDEF6B443D6ED1BA89399F05AEC81423D64C107B (void);
// 0x00000071 System.Object DestroyTime/<CallDestroy>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCallDestroyU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97E34B5CF7FEDA8B2A9EFBCE21A197CE51A2BB2 (void);
// 0x00000072 System.Void DestroyTime/<CallDestroy>d__2::System.Collections.IEnumerator.Reset()
extern void U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_Reset_mEAC272124FCDF01C2C344D034F9CBA6F076E2EF9 (void);
// 0x00000073 System.Object DestroyTime/<CallDestroy>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_get_Current_m89A18098EC6562CFAFAD044268F750C304307BA0 (void);
// 0x00000074 System.Void Explosion::Start()
extern void Explosion_Start_m519BD45EC393F52D86FB64B10889D5CBADCF4C22 (void);
// 0x00000075 System.Collections.IEnumerator Explosion::Explode()
extern void Explosion_Explode_m0586F5F99D95A44520E522FA9CF6256DCB7FC258 (void);
// 0x00000076 System.Void Explosion::.ctor()
extern void Explosion__ctor_m1400515C43124E852380BB8283E15042AF0A5094 (void);
// 0x00000077 System.Void Explosion/<Explode>d__3::.ctor(System.Int32)
extern void U3CExplodeU3Ed__3__ctor_mA6402CE95E7EDCBD469BF9A8CE6EE0321AC1C77F (void);
// 0x00000078 System.Void Explosion/<Explode>d__3::System.IDisposable.Dispose()
extern void U3CExplodeU3Ed__3_System_IDisposable_Dispose_m6323FDA8CD6A90728B774930FBE5BE77024148E8 (void);
// 0x00000079 System.Boolean Explosion/<Explode>d__3::MoveNext()
extern void U3CExplodeU3Ed__3_MoveNext_m7FF575367596C970A277CCA7BB7B3D994F32DADC (void);
// 0x0000007A System.Object Explosion/<Explode>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExplodeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86DF7C5A40A55D07317D9A2E9F52DC0BCB4C7D02 (void);
// 0x0000007B System.Void Explosion/<Explode>d__3::System.Collections.IEnumerator.Reset()
extern void U3CExplodeU3Ed__3_System_Collections_IEnumerator_Reset_m487EB8D5E237996B6DEF819E4290C05128D890D7 (void);
// 0x0000007C System.Object Explosion/<Explode>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CExplodeU3Ed__3_System_Collections_IEnumerator_get_Current_mFD18D2F6818A63E397EFE2758F218DABF2B2F2CB (void);
// 0x0000007D Stick/stck Stick::GetStck()
extern void Stick_GetStck_m7A49B714FF83A4D429439940FB6990888867BD7B (void);
// 0x0000007E System.Void PauseButton::pauseGame()
extern void PauseButton_pauseGame_mBC5D6398D4C3730275E58C9D0494C352BF67705B (void);
// 0x0000007F System.Void PauseButton::.ctor()
extern void PauseButton__ctor_m530D4B2903CE49586AA66126C0087CD0DD739B9F (void);
// 0x00000080 System.Void ControlShip::Start()
extern void ControlShip_Start_m5B912D419F20ED9BAFF9C7E0981D6ECF66DBA9F8 (void);
// 0x00000081 System.Void ControlShip::LateUpdate()
extern void ControlShip_LateUpdate_m8960B65ECF9A86E6205A72BCA648E6A8BF4EDF7D (void);
// 0x00000082 System.Void ControlShip::AnimateMotor()
extern void ControlShip_AnimateMotor_m95E9E12E5AB0068BB68740D04C32C91B610BAA27 (void);
// 0x00000083 System.Collections.IEnumerator ControlShip::Shoot()
extern void ControlShip_Shoot_mC367083E502F3FB24057683528873F3C1ABE49FA (void);
// 0x00000084 System.Void ControlShip::CallShield()
extern void ControlShip_CallShield_m981741EAE83CD99702ECA0E88C92115E68E6CA0C (void);
// 0x00000085 System.Void ControlShip::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void ControlShip_OnCollisionEnter2D_mF0FEC752B221AC776F36E9EB533C3AAD666FBC73 (void);
// 0x00000086 System.Void ControlShip::.ctor()
extern void ControlShip__ctor_m1DFE695CD1EAB1B2CB9B079E3ED024FF986CFC93 (void);
// 0x00000087 System.Void ControlShip/<Shoot>d__15::.ctor(System.Int32)
extern void U3CShootU3Ed__15__ctor_m2B8AEC7F4C367018734F07D04F33AF1D457AECB4 (void);
// 0x00000088 System.Void ControlShip/<Shoot>d__15::System.IDisposable.Dispose()
extern void U3CShootU3Ed__15_System_IDisposable_Dispose_m5A84EFE10951A4AB77CC01EB739D428507BC9D61 (void);
// 0x00000089 System.Boolean ControlShip/<Shoot>d__15::MoveNext()
extern void U3CShootU3Ed__15_MoveNext_m9EEB87C1C824C95039077A22A766F5481F7FBDF5 (void);
// 0x0000008A System.Object ControlShip/<Shoot>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShootU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m70974FE0B089361CAD462BCE365A3FB4E9337C4B (void);
// 0x0000008B System.Void ControlShip/<Shoot>d__15::System.Collections.IEnumerator.Reset()
extern void U3CShootU3Ed__15_System_Collections_IEnumerator_Reset_m10AA0124EE17DB5A2A71839446D7DD301A7FC818 (void);
// 0x0000008C System.Object ControlShip/<Shoot>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CShootU3Ed__15_System_Collections_IEnumerator_get_Current_m9263C839D099A97D92EA9E46C93CDBCD50FE96B5 (void);
// 0x0000008D System.Void Flux.ReadMe::.ctor()
extern void ReadMe__ctor_m8FF25B75AB71F389F93E1E2A4DE7D2985ADD6F56 (void);
// 0x0000008E System.Void Gaminho.Level::.ctor()
extern void Level__ctor_mADE87462A9D13B244DC3F73F22BBA57A3CF8ADD9 (void);
// 0x0000008F System.Void Gaminho.ScenarioLimits::.ctor()
extern void ScenarioLimits__ctor_mE7ED76B4AD2650003AE4F5A075F9D7F03360730B (void);
// 0x00000090 System.Void Gaminho.Shot::.ctor()
extern void Shot__ctor_m4BFE80FB650FFA47A20F832227B0A5F9BD84C4B3 (void);
// 0x00000091 UnityEngine.Quaternion Gaminho.Statics::FaceObject(UnityEngine.Vector2,UnityEngine.Vector2,Gaminho.Statics/FacingDirection)
extern void Statics_FaceObject_mD7C4A0B189730DDA2CC972CE3204E2FE896FAF50 (void);
// 0x00000092 System.Void Gaminho.Statics::.cctor()
extern void Statics__cctor_mB0BED0D7AA2F076698019E572C2CB7E7FEFC3BA4 (void);
static Il2CppMethodPointer s_methodPointers[146] = 
{
	Life_TakesLife_m432769A0DBB59D454784B3402F19D358B66CBDBD,
	Life_OnTriggerEnter2D_mB5E33226D7DB91144ACC54786FDF08388B017178,
	Life__ctor_m7EA475E113F8E58CDBD7F64DE7C3378510DD7926,
	ShieldLife_setMaxShield_mC8E0EF88AA7A04B33BCB5831794D9F629C71FF7F,
	ShieldLife_setShield_m9495FF985111077CF0682E50324F8C5DFFC52907,
	ShieldLife__ctor_mE4A6C282A2616BA5D40F1CC8CB105E50386A0CBB,
	ControlGame_Start_m41EDFA823653EE35B75C9E396B158F9C47330225,
	ControlGame_Update_m75CA34A327D9AE3F5BE315C068BB2D3946985B50,
	ControlGame_LevelPassed_m05B6235DD8F835987CA547397293A735BB6F6534,
	ControlGame_GameOver_m1E16954A2EB5306CA5D7959354BD4728C0321A13,
	ControlGame_Clear_m0681179893AC0309B4E59F5EA1033BE470C10074,
	ControlGame__ctor_mBB7334A9B707AA5AB4EC6F7C853BE92433BB4D4D,
	ControlStart_Start_m201D1963F24351E276FC706916096DE032376F51,
	ControlStart_StartClick_m4F919D6E668B781C8C5DC0EB2EB21611D882EFAB,
	ControlStart_Quit_mD819D0CAD1831F9E534C96E210F5FFFF9FDB1747,
	ControlStart__ctor_mB1F89FE5B8F09B1F79FC79EA5BFDBDCEBEF790EA,
	EnemyControl_Start_mB2709A00C33F64E801FB336D9224A48BAEDF451F,
	EnemyControl_Process_m3026B4978A8349BD60037F2B25AFA4539B70FB7B,
	EnemyControl_EnemyCreate_m1DBC2B3B2C19C6C6D410EAB1360BB814DD12FD3E,
	EnemyControl_CallBoss_mEE1F75478CABCE55BC6A16FA00D0F843E7A58101,
	EnemyControl__ctor_mE15FA6B97AEFE883D943CC0D237897F435BC34BF,
	U3CProcessU3Ed__4__ctor_mA6DCD4EAD0188CA4C4AD0A5EF0D2A9FC2F8ADC15,
	U3CProcessU3Ed__4_System_IDisposable_Dispose_m382FC3C37C2241AC4E64B8D0EC2C821F916C3DE2,
	U3CProcessU3Ed__4_MoveNext_m3A08B74C2B6D96C327D949D051A8EA8D5B423DDC,
	U3CProcessU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m277414C234EE05878133959D21839B5949AC295B,
	U3CProcessU3Ed__4_System_Collections_IEnumerator_Reset_mE513AF299BB9BF436B27408FF4C0558A73F4676D,
	U3CProcessU3Ed__4_System_Collections_IEnumerator_get_Current_m158EF3608628631AC8F9A0A6CE2568AEB6566996,
	U3CCallBossU3Ed__6__ctor_mDA19F5EC5865BAF920AD4E709D8ADE95E743F363,
	U3CCallBossU3Ed__6_System_IDisposable_Dispose_m68D5010133251BF57F27F51D1BB928B1E4C8DDD9,
	U3CCallBossU3Ed__6_MoveNext_mDF3544E995D32004659624A65FB35CCD76C0B909,
	U3CCallBossU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2297F4117C95DDE24CFD237A355224230FA00BD9,
	U3CCallBossU3Ed__6_System_Collections_IEnumerator_Reset_mAB176BC3F3DE4AD36A0EAF25D234733D5CA47B3D,
	U3CCallBossU3Ed__6_System_Collections_IEnumerator_get_Current_mB515EAE6DED7E678D67D764943739ADEF1C84D27,
	RecordControl_Start_m8F7A53F239DE398E63E55AA2A9EF390392EB516F,
	RecordControl_UpdateName_m3C7C5ABFF677519820B6F281AF99A768064360DC,
	RecordControl__ctor_m8B9BD294AC3D1EDA3869E09F7EFC58F7B97304C4,
	Boss2_Start_mF5292CA924E8DB443334DB78CCD7201F768E15C8,
	Boss2_ShowParts_mCDC314F0FBDB8D208165FAAE3F8F4D358F7FD315,
	Boss2_Update_m8CE583E0DAC32C0A374F69542AD8C054754C0C14,
	Boss2_Shot_m35B8B00F7AEF10F996EF1D55BEF3575E5BAFF3D6,
	Boss2_GetOneActive_m41CEB8C5D4F936E741F9C9F684F40EAE7D3DADA3,
	Boss2_KillMe_mDE3ED52B5D597F8CEB197D98E1F15E9334C71285,
	Boss2__ctor_mAF0CEE4D38FEAFC1DE100B0F2E0701E06B14991F,
	U3CShowPartsU3Ed__5__ctor_mBDD0E8898D91E138B7042D22F0A5E1456EA501C8,
	U3CShowPartsU3Ed__5_System_IDisposable_Dispose_m0AC2D2E39174DA2D173BDA8759091EEE1D71F146,
	U3CShowPartsU3Ed__5_MoveNext_m0B40D4DCC0A1EBAC248878B225A64D4925E31704,
	U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F4521C1346516C77ED9C7B550671F928987CE41,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m0468FE7B0426062B26C538D23C7C5FF8CC38570D,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_m20F3D7FA705381FA5AA9C2B5C037BDCB2BC2128A,
	U3CShotU3Ed__7__ctor_mE5DADDE0420441F86985C44BD718DD08A4AD334C,
	U3CShotU3Ed__7_System_IDisposable_Dispose_m8F588491D89FF21288555692FE3A90E505E341F7,
	U3CShotU3Ed__7_MoveNext_m6010D4868EF31AE0774066824F01233F5F37D4AE,
	U3CShotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12B604A9AC558372662A0FFD180A325ACDD91EA6,
	U3CShotU3Ed__7_System_Collections_IEnumerator_Reset_mE289E17958922FDDCC8DE7C1FA61940025274E62,
	U3CShotU3Ed__7_System_Collections_IEnumerator_get_Current_m935EF7A6B9356B060AA9DFD3E78AA608C890BC25,
	Boss3_Start_m025FF1DBDC22B6304FBDBAD41E2A028B5BCF4E2C,
	Boss3_ShowParts_m237CC1FA5AE12E3DC8B0F2E864759C88F37E1EC6,
	Boss3_AttackNow_m751CAFCD3CD70F4748E1BE4A61688B418BF83420,
	Boss3_Attack_mBC827254E14F721046A48C74FBDCE480D7EB8006,
	Boss3_GetOneActive_mC454EE4919EB35E3CADF35A2C047B267CBB5754F,
	Boss3_KillMe_m485F8403E9462C6F16E4A1E1346373AE8AE15029,
	Boss3__ctor_m643E6AD2933C0D491F1706E2F4CA3BA2F3820306,
	U3CShowPartsU3Ed__4__ctor_m070F5DA201A9A67A2D4EB009D50CEFA4CDF325CD,
	U3CShowPartsU3Ed__4_System_IDisposable_Dispose_m095CFB0C977C6035FE63D564EA6AB2576AF732F9,
	U3CShowPartsU3Ed__4_MoveNext_m04E0F92B3885C3D0069909C43E601567B8254F35,
	U3CShowPartsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3AB3BC8537AF41519422CE94D13E117A2055BF4E,
	U3CShowPartsU3Ed__4_System_Collections_IEnumerator_Reset_m9C7BFE5CCD4D8826B79B86BA66548260D7BBDE04,
	U3CShowPartsU3Ed__4_System_Collections_IEnumerator_get_Current_m1877C089084E4ED7D969D798AE14C8CEF88C76C6,
	U3CAttackNowU3Ed__5__ctor_mCFE69ED94B71BE7330D7DBD55F859132EFA4D2F3,
	U3CAttackNowU3Ed__5_System_IDisposable_Dispose_m89B4264BD52CD77D67EA94A14771AB7F55DEF46E,
	U3CAttackNowU3Ed__5_MoveNext_m12010287B84893CC8526EBD2A04624722F2290D0,
	U3CAttackNowU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4FFA34D632DB5A73E42B8EAD8D4861CA70617CF3,
	U3CAttackNowU3Ed__5_System_Collections_IEnumerator_Reset_mAC62F325D898ABA3D347C2BE92E5A14D0B9299B0,
	U3CAttackNowU3Ed__5_System_Collections_IEnumerator_get_Current_m132D1DD78E6BC1C54A09ED6F8AE816E2EFD154A2,
	U3CAttackU3Ed__6__ctor_mA4F7019DA5183D416ADAAF260A4C975A61B43FDA,
	U3CAttackU3Ed__6_System_IDisposable_Dispose_m4A06ED756364F4E6E18CF74382173F30DA1633E3,
	U3CAttackU3Ed__6_MoveNext_mBA62607420F65E09A22036D8DD18C7F73B4F0AE2,
	U3CAttackU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F2BD237C0E43E3D21F19E3DDE8AF264B68E03DE,
	U3CAttackU3Ed__6_System_Collections_IEnumerator_Reset_m3555C671D642A35E37C61E8B3199D7C54B6C3F79,
	U3CAttackU3Ed__6_System_Collections_IEnumerator_get_Current_m2AA0E8B6B15E5E3BB4B4FE4A985F94E33AD15757,
	Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02,
	Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2,
	Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12,
	Enemy_MyDeath_mA4085ED4BD10E6B3EDEE186807D3C8FB762A4CC8,
	Enemy_KillMe_m24D0CC29C673187902BFD4554072865ADF3F2EB5,
	Enemy_Create_m66ACA885B980722A913D038190E87E6AFFA55759,
	Enemy_Shoot_m9698649D8FCE4A907B5EBAA30105E3DF881FB767,
	Enemy_EndBoss_mA358579DEF2D3DAF68CD80B9AD4C96C1981554DA,
	Enemy_PStick_m5BAC91E00684770270D3B5164328B2EF020D9173,
	Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734,
	U3CKillMeU3Ed__10__ctor_mCBE3F42CF5E6BEB5E774F037B0444C559C24EC4C,
	U3CKillMeU3Ed__10_System_IDisposable_Dispose_m2DB73B1C1F038ACD13017517E648F7BC34FE75A7,
	U3CKillMeU3Ed__10_MoveNext_m2B827634D8CEADE16F1ABA524332C1D91AAEEA08,
	U3CKillMeU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB162D3E0999042813FBE550CD7FD7926C498BF3C,
	U3CKillMeU3Ed__10_System_Collections_IEnumerator_Reset_m144E9C6610E020F7905EF9E9E55E6D769047778C,
	U3CKillMeU3Ed__10_System_Collections_IEnumerator_get_Current_m89F0A6CE9070973A816212BD6572DA6DC9D9F303,
	U3CShootU3Ed__12__ctor_m4884E34148F61D16DCBE9E6C9E29CF766636E036,
	U3CShootU3Ed__12_System_IDisposable_Dispose_m9E7ADDC293233521A708C4B1114000B969F4F6E4,
	U3CShootU3Ed__12_MoveNext_mD258E3C08A9B604CFA4F85EFA5B6E32189C9EC55,
	U3CShootU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3E2421A685652897F0F5F6D05DA25B8ACDEE5DD,
	U3CShootU3Ed__12_System_Collections_IEnumerator_Reset_m5C552696876E029CE03DD55B746CD7422330D2DF,
	U3CShootU3Ed__12_System_Collections_IEnumerator_get_Current_m9D53A9EAB026124E6E8AF1E6BC87C3375A604866,
	ItemDrop_OnTriggerEnter2D_m1A1B2CBD8DABD7C2C8C666248C9E7B9A56F022A7,
	ItemDrop__ctor_m5D826B2329C00417D6FB12D4C40DE3EB0DB55AF2,
	CallScene_Call_m8CDF9D77505FAEDF29068B890862D09654434CCE,
	CallScene__ctor_m6B52AD5FB87324D006A0BFF36D122D8E5096AE9C,
	DestroyTime_Start_m8C0A193A37746E7A2A2C617E998A31E724373497,
	DestroyTime_CallDestroy_m61D5F2B05283C2474DC66285AC88FDA79B8E1BD6,
	DestroyTime__ctor_mAB5E312F1EAAD1B3B69CC64F2B95B607F8EE4F88,
	U3CCallDestroyU3Ed__2__ctor_m7A7A8DC06CB12FA979DC587DA59CD00B21710FAF,
	U3CCallDestroyU3Ed__2_System_IDisposable_Dispose_m93E09EB5836B606D9F59C3CB667BDD2C8F1215BD,
	U3CCallDestroyU3Ed__2_MoveNext_mCDEF6B443D6ED1BA89399F05AEC81423D64C107B,
	U3CCallDestroyU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97E34B5CF7FEDA8B2A9EFBCE21A197CE51A2BB2,
	U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_Reset_mEAC272124FCDF01C2C344D034F9CBA6F076E2EF9,
	U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_get_Current_m89A18098EC6562CFAFAD044268F750C304307BA0,
	Explosion_Start_m519BD45EC393F52D86FB64B10889D5CBADCF4C22,
	Explosion_Explode_m0586F5F99D95A44520E522FA9CF6256DCB7FC258,
	Explosion__ctor_m1400515C43124E852380BB8283E15042AF0A5094,
	U3CExplodeU3Ed__3__ctor_mA6402CE95E7EDCBD469BF9A8CE6EE0321AC1C77F,
	U3CExplodeU3Ed__3_System_IDisposable_Dispose_m6323FDA8CD6A90728B774930FBE5BE77024148E8,
	U3CExplodeU3Ed__3_MoveNext_m7FF575367596C970A277CCA7BB7B3D994F32DADC,
	U3CExplodeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86DF7C5A40A55D07317D9A2E9F52DC0BCB4C7D02,
	U3CExplodeU3Ed__3_System_Collections_IEnumerator_Reset_m487EB8D5E237996B6DEF819E4290C05128D890D7,
	U3CExplodeU3Ed__3_System_Collections_IEnumerator_get_Current_mFD18D2F6818A63E397EFE2758F218DABF2B2F2CB,
	Stick_GetStck_m7A49B714FF83A4D429439940FB6990888867BD7B,
	PauseButton_pauseGame_mBC5D6398D4C3730275E58C9D0494C352BF67705B,
	PauseButton__ctor_m530D4B2903CE49586AA66126C0087CD0DD739B9F,
	ControlShip_Start_m5B912D419F20ED9BAFF9C7E0981D6ECF66DBA9F8,
	ControlShip_LateUpdate_m8960B65ECF9A86E6205A72BCA648E6A8BF4EDF7D,
	ControlShip_AnimateMotor_m95E9E12E5AB0068BB68740D04C32C91B610BAA27,
	ControlShip_Shoot_mC367083E502F3FB24057683528873F3C1ABE49FA,
	ControlShip_CallShield_m981741EAE83CD99702ECA0E88C92115E68E6CA0C,
	ControlShip_OnCollisionEnter2D_mF0FEC752B221AC776F36E9EB533C3AAD666FBC73,
	ControlShip__ctor_m1DFE695CD1EAB1B2CB9B079E3ED024FF986CFC93,
	U3CShootU3Ed__15__ctor_m2B8AEC7F4C367018734F07D04F33AF1D457AECB4,
	U3CShootU3Ed__15_System_IDisposable_Dispose_m5A84EFE10951A4AB77CC01EB739D428507BC9D61,
	U3CShootU3Ed__15_MoveNext_m9EEB87C1C824C95039077A22A766F5481F7FBDF5,
	U3CShootU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m70974FE0B089361CAD462BCE365A3FB4E9337C4B,
	U3CShootU3Ed__15_System_Collections_IEnumerator_Reset_m10AA0124EE17DB5A2A71839446D7DD301A7FC818,
	U3CShootU3Ed__15_System_Collections_IEnumerator_get_Current_m9263C839D099A97D92EA9E46C93CDBCD50FE96B5,
	ReadMe__ctor_m8FF25B75AB71F389F93E1E2A4DE7D2985ADD6F56,
	Level__ctor_mADE87462A9D13B244DC3F73F22BBA57A3CF8ADD9,
	ScenarioLimits__ctor_mE7ED76B4AD2650003AE4F5A075F9D7F03360730B,
	Shot__ctor_m4BFE80FB650FFA47A20F832227B0A5F9BD84C4B3,
	Statics_FaceObject_mD7C4A0B189730DDA2CC972CE3204E2FE896FAF50,
	Statics__cctor_mB0BED0D7AA2F076698019E572C2CB7E7FEFC3BA4,
};
static const int32_t s_InvokerIndices[146] = 
{
	1229,
	1240,
	1445,
	1229,
	1229,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1413,
	1445,
	1413,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1445,
	1445,
	1445,
	1445,
	1413,
	1445,
	1413,
	1413,
	1240,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1445,
	1006,
	1413,
	1005,
	1413,
	1240,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1445,
	1445,
	1240,
	1445,
	1413,
	1229,
	1413,
	1445,
	1445,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1240,
	1445,
	1240,
	1445,
	1445,
	1413,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1445,
	1413,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	2341,
	1445,
	1445,
	1445,
	1445,
	1445,
	1413,
	1445,
	1240,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1445,
	1445,
	1445,
	1445,
	1873,
	2362,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	146,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
