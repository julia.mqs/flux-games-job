# Flux Games Job

Delivery of the completed tasks as asked on the programming test for Flux Games.

Includes:
- Original Unity source (Find Job folder);
- Regular Windows build (PC folder inside Builds);
- 2 versions of UWP build (UWP and UWP 2 folders inside Builds - they were built in slightly different ways in Unity).
